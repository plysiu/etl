<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>ETL</title>
        <?php echo Asset::css('bootstrap.css'); ?>
        <style>
            #logo{
                display: block;
                width: 179px;
                height: 45px;
                position: relative;
                top: 15px;
            }
            #header{
                height: 75px;
                width: 100%;
                margin-bottom: 40px;
            }
            #header .row{
                width: 940px;
                margin: 0 auto;
            }
            a{
                color: #883ced;
            }
            a:hover{
                color: #af4cf0;
            }
            .btn.btn-primary{color:#ffffff!important;background-color:#883ced;background-repeat:repeat-x;background-image:-khtml-gradient(linear, left top, left bottom, from(#fd6ef7), to(#883ced));background-image:-moz-linear-gradient(top, #fd6ef7, #883ced);background-image:-ms-linear-gradient(top, #fd6ef7, #883ced);background-image:-webkit-gradient(linear, left top, left bottom, color-stop(0%, #fd6ef7), color-stop(100%, #883ced));background-image:-webkit-linear-gradient(top, #fd6ef7, #883ced);background-image:-o-linear-gradient(top, #fd6ef7, #883ced);background-image:linear-gradient(top, #fd6ef7, #883ced);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#fd6ef7', endColorstr='#883ced', GradientType=0);text-shadow:0 -1px 0 rgba(0, 0, 0, 0.25);border-color:#883ced #883ced #003f81;border-color:rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
                             body { margin: 0px 0px 40px 0px; }
            </style>
        </head>
        <body>
            <div id="header">
            <div class="row">
                <div id="logo"></div>
            </div>
        </div>
        <div class="container">
            <div class="hero-unit">
                <h1>Welcome to ETL xD!</h1>
                <p><?= Session::get_flash('message') ?></p>

            </div>
            <? if (isset($data)): ?>
                <div class="row">
                    <div class="span12">
                        <table class="table">
                            <thead>
                                <tr><th>Symbol</th><th>Description</th><tr>
                            </thead>
                            <tbody>
                                <? foreach ($data as $row): ?>
                                    <tr><td><?= $row['symbol'] ?></td><td><?= $row['description'] ?></td></tr>
                                <? endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            <? endif; ?>
            <div class="row">

                <div class="offset4 span9">
                    <div class="btn-group"
                         >
                             <?= \Fuel\Core\Html::anchor('welcome/extract', 'Extract', array('class' => 'btn')) ?>
                             <?= \Fuel\Core\Html::anchor('welcome/transform', 'Transform', array('class' => 'btn')) ?>
                             <?= \Fuel\Core\Html::anchor('welcome/load', 'Load', array('class' => 'btn')) ?>
                             <?= \Fuel\Core\Html::anchor('welcome/etl', 'ALl in One', array('class' => 'btn')) ?>
                             <?= \Fuel\Core\Html::anchor('welcome/view', 'View', array('class' => 'btn')) ?>
                    </div>
                </div>
            </div>
            <hr/>
            <footer>
                <p class="pull-right">Page rendered in {exec_time}s using {mem_usage}mb of memory.</p>
                <p>
                    <a href="http://fuelphp.com">FuelPHP</a> is released under the MIT license.<br>
                    <small>Version: <?php echo Fuel::VERSION; ?></small>
                </p>
            </footer>
        </div>
    </body>
</html>
