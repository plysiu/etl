<?php

namespace Etl;

/**
 * Klasa odpowiada za transformacje danych do ziunifikowanych typów danych
 */
class Transform {

    /**
     * Przechowuje obiekt z zawartością danych przed transformacją
     * @var \Fuel\Core\Format
     */
    protected $data;

    /**
     * Inicjuje obiekt Transform
     * @param Extract $extract Obiekt typu Extract
     * @param array $filtr Tablica z numerami indeksów elementów które mają zostać wyświetlone
     */
    public function __construct($extract, $filtr = array()) {

        try {
            $this->data = \Cache::get('transform');
        } catch (\CacheNotFoundException $e) {

            $data = $extract->get_data();
            for ($i = 1; $i < count($extract->get_data()) - 1; $i++) {
                foreach ($data[$i] as $key => $value) {
                    if (count($filtr) == 0 || in_array($i, $filtr))
                        $array[$key][] = $value;
                }
            }
            $this->data = \Cache::set('transform', \Fuel\Core\Format::forge($array));
        }
    }

    /**
     * Zwraca dane w postaci csv
     * @return String Format CSV
     */
    public function get_csv() {
        return $this->data->to_csv();
    }

    /**
     * Zwraca dane w postaci tablicy
     * @return array For    mat talbicy PHP
     */
    public function get_array() {
        return $this->data->to_array();
    }

    /**
     * Zwraca dane w postaci json
     * @return string Format JSON
     */
    public function to_json() {
        return $this->data->to_json();
    }

}

