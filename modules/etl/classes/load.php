<?php

namespace Etl;

/**
 * Klasa jest odpowiedzialna za wprowadzenie danych do bazy danych
 */
class Load {

    /**
     * Flaga do sprawdania czy jest jakaś wartość do wprowadzenia do bazy danych
     * @var boolean
     */
    private $flag = false;

    /**
     * Przechowuje połaczenie z baza danych
     * @var \Fuel\Core\DB Obiekt połaczenia z bazą danych
     */
    private $query = null;

    /**
     * Inicjuje obiekt przygotowuje liste danych do zapisu do bazy danych
     * @param \Etl\Transform $transform Obiekt Transform z danymi
     * @param array $fields Pola jakie są w tej tabeli
     * @param int $pk Nazwa pola klucza głównego
     * @param string $table Nazwa tabeli
     */
    public function __construct(Transform $transform, $fields, $pk, $table) {

        // Pobieramy istniejącą liste kluczy głównych
        $result = \DB::select($fields[--$pk])->from($table)->as_assoc()->execute();

        // przechodzimy po każdym elemencie dostarczonym z obiektu Transform
        foreach ($transform->get_array() as $row) {
// Sprawdzamy czy nie istnieje już element w bazie o takim samym symbolu
            if (!array_key_exists($row[0], $result->as_array($fields[$pk]))) {

                if (!$this->flag) {

                    $this->flag = true;
                    $this->query = \DB::insert($table)->columns($fields);
                }
                $this->query->values($row);
            }
        }
    }

    /**
     * Wywołuje zapytanie
     * @return boolean Zawsze zwraca prawde
     */
    public function execute() {

        if ($this->flag) {
            // Wykonujemy zapytanie w kt¶óe wprowadza dane do bazy danych
            $this->query->execute();
        }
        return true;
    }

}

