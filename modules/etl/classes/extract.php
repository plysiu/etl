<?php

namespace Etl;

/**
 * Klasa jest odpowiedzialna zunifikowanie procesu pobierania danych przed ich transformacją
 */
class Extract {

    /**
     * Przechowuje pobrane dane
     * @var string
     */
    protected $request_data = null;

    /**
     * Przechowuje wyłuskane dane
     * @var array
     */
    protected $data = array();

    /**
     * Inicjuje obiekt, wykonuje zapytania i je cacheuje
     * @param \Request $request
     */
    public function __construct($requests = array()) {

        /* @var $request \Fuel\Core\Request_Curl|\Fuel\Core\Request_Soap */
        foreach ($requests as $address => $request) {
            try {
                $this->request_data .= \Cache::get(md5($address));
            } catch (\CacheNotFoundException $e) {
                $this->request_data .= \Cache::set(md5($address), $request->execute()->response());
            }
        }
    }

    /**
     * Zwraca prawdę jeśli wyłuskiwanie za pomocą wyrażenia znajdzie przynajmniej jedno wyrażenie odpowiadające parametrowi
     * @return boolean
     */
    public function execute($reg_exp) {
        return preg_match_all($reg_exp, $this->request_data, $this->data);
    }

    /**
     * Zwraca tablice wraz z elementami wyłuskanymi
     * @return array Tablica z elementami po wyłuskiwaniu z a pomocą preg_match_all
     */
    public function get_data() {


        return $this->data;
    }

}

