<?php

namespace Fuel\Migrations;

/**
 * Klasa migracyjna modelu danych etl
 */
class Etl {

    /**
     * Tworzy tabele w bazie danych
     */
    function up() {
        \DBUtil::create_table('Market', array(
            'symbol' => array('type' => 'varchar','constraint' => 100),
            'description' => array('type' => 'text')
                ), array('symbol'));
    }

    /**
     * Usuwa tabele w bazie danych
     */
    function down() {
        \DBUtil::drop_table('Market');
    }

}

