<?php

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */
class Controller_Welcome extends Controller {

    /**
     * Domyślna strona
     *
     * @access  public
     * @return  Response
     */
    public function action_index() {
        return Response::forge(View::forge('welcome/index'));
    }

    /**
     * Strona błędu 404
     *
     * @access  public
     * @return  Response
     */
    public function action_404() {

        return Response::forge(ViewModel::forge('welcome/404'), 404);
    }

    /**
     * Pobiera strony i wybiera z nich dane
     * @return boolean|\Etl\Extract
     */
    public function extract() {

        $markets = array('NYSE', 'NASDAQ', 'AMEX');
        $symbols = array('A', 'B', 'C');

        foreach ($markets as $market) {
            foreach ($symbols as $symbol) {
                $requests['http://www.findata.co.nz/markets/' . $market . '/symbols/' . $symbol . '.htm'] =
                        Request::forge('http://www.findata.co.nz/markets/' . $market . '/symbols/' . $symbol . '.htm', 'curl');
            }
        }

        $extract = new Etl\Extract($requests);

        $check = $extract->
                execute("/<td.*?><A href.*?>(.*?)<\/A><\/td><td.*?>(.*?)<\/td><td.*?>(.*?)<\/td><td.*?>(.*?)<\/td><td.*?>(.*?)<\/td><td.*?>(.*?)<\/td><td.*?>(.*?)<\/td><td.*?><IMG.*?<\/td><td.*?>(.*?)<\/td><td.*?>&nbsp;(.*?)&nbsp;<\/td>/");
        if ($check) {

            Session::set_flash('message', 'Ekstrakcja danych została wykonana poprawnie.');

            return $extract;
        }
        Session::set_flash('message', 'Ekstrakcja danych została wykonana nie poprawnie.');
        return false;
    }

    /**
     * Strona wywołuje pobieranie i wyłuskiwanie danych
     *
     */
    public function action_extract() {

        $this->extract();
        Fuel\Core\Response::redirect();
    }

    /**
     * Transformuje dane
     * @return \Etl\Transform|boolean
     */
    public function transform() {

        $extract = $this->extract();
        if (is_a($extract, '\Etl\Extract')) {

            Session::set_flash('message', 'Transformacja danych została wykonana.');
            return new Etl\Transform($extract, array(1, 2));
        }
        Session::set_flash('message', 'Transformacja danych nie została wykonana.');
        return false;
    }

    /**
     * Strona wywołuje transformacje danych
     */
    public function action_transform() {
        $this->transform();
        Fuel\Core\Response::redirect();
    }

    /**
     * Ładuje dane do bazy danych
     * @return boolean
     */
    public function load() {

        $transform = $this->transform();
        if (is_a($transform, '\Etl\Transform')) {

            $load = new \Etl\Load($transform, array('symbol', 'description'), 1, 'Market');

            if ($load->execute()) {
                Session::set_flash('message', 'Ładowanie danych zostało wykonane poprawnie.');
                Cache::delete_all();
                return true;
            }
        }
        Session::set_flash('message', 'Podczas ładowania danych wystąpił błąd.');
        return false;
    }

    /**
     * Strona wywołuje metode ładowania danych do bazy danych
     */
    public function action_load() {

        $this->load();

        Fuel\Core\Response::redirect();
    }

    /**
     * CZysty ETL wyłuskuje przetwarza iładuje dane do bazy danych
     */
    public function action_etl() {

        $this->load();
        Fuel\Core\Response::redirect();
    }

    /**
     * Wyświetla 15 poczatkowych wierszy z bazy danych
     * @return Response
     */
    public function action_view() {

        $result = DB::query('SELECT * FROM `Market` LIMIT 15', DB::SELECT)->execute();

        return Response::forge(View::forge('welcome/index', array('data' => $result->as_array())));
    }

}
